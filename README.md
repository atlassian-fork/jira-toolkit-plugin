# Jira Toolkit Plugin
##### This is Jira server version 

Jira Toolkit Plugin is adding additional custom fields to Jira. The toolkit is a set of neat custom fields Atlassian developed for their own use, particularly when using Jira for customer support. They provide a simple workaround for current limitations to Jira or solutions to some idiosyncratic problems. Think of it as Jira Custom fields sandbox if you will. It's also a showcase for how simple custom fields can be used get more out of your of Jira, especially for filtering on information that would otherwise be unavailable. 
Fields can be managed [here.](/secure/admin/CreateCustomField!default.jspa)

### Branching / Versioning

| branch        | version      |   compatibility  | builds |
| ------------- |:------------:|-------:| -----:|
| master | 0.36.x | Jira 8.x | [Build](https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINS-JIRATOOLKITPLUGIN/) [Build OpenJDK 8](https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINSOPENJDK8-JIRATOOLKITPLUGIN) [Build OpenJDK 11](https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINSOPENJDK11-JIRATOOLKITPLUGIN/)|
| atlassian_jira_7_x_branch | 0.35.x (>= 0.35.13)| Jira 7.x | [Build](https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINS-JIRATOOLKITPLUGIN/) |


### Marketplace

Plugin is available on [marketplace](https://marketplace.atlassian.com/manage/apps/5142/versions)

### Plan templates

Plan templates are available [here](https://stash.atlassian.com/projects/JIRASERVER/repos/jira-plan-templates/browse/src/main/groovy/com/atlassian/jira/plan_templates/plugins/JiraToolkitPlugin.groovy).   

### Cloud version

The Cloud version of this plugin now lives inside JIRA repo.

### Links
* [Issues](https://ecosystem.atlassian.net/projects/JTOOL/issues/JTOOL-149?filter=allopenissues)
