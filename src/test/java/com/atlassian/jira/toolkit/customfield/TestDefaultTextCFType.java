/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.mockobjects.dynamic.Mock;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultTextCFType extends TestCase {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    private Mock customFieldValuePersister;
    private Mock genericConfigManager;
    private MockFeatureManager featureManager;
    private Mock rendererManager;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        customFieldValuePersister = new Mock(CustomFieldValuePersister.class);
        genericConfigManager = new Mock(GenericConfigManager.class);
        featureManager = new MockFeatureManager();
        rendererManager = new Mock(RendererManager.class);
    }

    private void commonAssertions(String value, String expecting) {
        final Map<String, Object> params = new HashMap<String, Object>();
        DefaultTextCFType defaultTextCFType = new DefaultTextCFType((CustomFieldValuePersister) customFieldValuePersister.proxy(), (GenericConfigManager) genericConfigManager.proxy(), featureManager, (RendererManager) rendererManager.proxy());
        defaultTextCFType.setVelocityValueParametersValue(params, value);
        assertEquals(expecting, params.get("value"));
        assertEquals(expecting, params.get("defaultValue"));
    }

    @Test
    public void testHtmlMarkup() {
        String value = "<image src='http://example.com/image.jpg'>";
        String expecting = value;
        commonAssertions(value, expecting);
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
        JiraAuthenticationContextImpl.getRequestCache().clear();
    }

}
