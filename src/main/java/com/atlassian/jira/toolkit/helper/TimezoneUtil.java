package com.atlassian.jira.toolkit.helper;

import java.util.TimeZone;
import java.util.Date;

public class TimezoneUtil {
    public static void main(String[] args) {
        System.out.println(getGMTStringFromTimezoneString(args[0]));
    }

    /**
     * Given a timezone string in any format Java understands, translate to GMT[+-]X, rounded to the nearest hour.
     *
     * @param tzRawString Eg. "Australia/Sydney".
     * @return Eg. "GMT+11". Will range from GMT-13 to GMT+13. For GMT, returns "GMT+0". If timezone is unrecognized,
     * returns "GMT+0".
     */
    public static String getGMTStringFromTimezoneString(String tzRawString) {
        TimeZone timeZone = TimeZone.getTimeZone(tzRawString); // returns GMT if it can't parse (yuck!)
        int offsetMS = timeZone.getRawOffset() + (timeZone.inDaylightTime(new Date()) ? timeZone.getDSTSavings() : 0);
        int offsetHour = offsetMS / 1000 / 60 / 60; // Note that this rounds timezones *down* to the nearest hour
        return "GMT" + (offsetHour >= 0 ? "+" : "") + offsetHour;
    }
}
